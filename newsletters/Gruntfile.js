module.exports = function (grunt) {
    'use strict';

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        /**
         * Project Paths Configuration
         * ===============================
         */
        paths: {
            //images folder name
            images: 'img',
            //where to store built files
            dist: 'dist',
            //sources
            src: 'app',
            //main email file
            email: 'valentines.html',
            // email: 'generic.html',
            //enter here yout production domain
            distDomain: 'http://www.sunshinefdc.com.au/',
            //this is the default development domain
            devDomain: 'http://<%= connect.options.hostname %>:<%= connect.dev.options.port %>/'
        },

        /**
         * SCSS Compilation Tasks
         * ===============================
         */
        compass: {
            options: {
                sassDir: '<%= paths.src %>/scss',
                outputStyle: 'expanded',
                httpImagesPath: '/img/'
            },
            dev: {
                options: {
                    cssDir: '<%= paths.src %>/css',
                    imagesDir: '<%= paths.src %>/<%= paths.images %>',
                    noLineComments: false
                }
            },
            dist: {
                options: {
                    force: true,
                    cssDir: '<%= paths.dist %>/css',
                    imagesDir: '<%= paths.dist %>/<%= paths.images %>',
                    noLineComments: true,
                    assetCacheBuster: false
                }
            }
        },

        /**
         * Watch Task
         * ===============================
         */
        watch: {
            compass: {
                files: ['<%= paths.src %>/scss/**/*.scss'],
                tasks: ['compass:dev']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= paths.src %>/<%= paths.email %>',
                    '<%= paths.src %>/css/{,*/}*.css',
                    '<%= paths.src %>/<%= paths.images %>/{,*/}*.{png,jpg,jpeg,gif}'
                ]
            }
        },

        /**
         * Server Tasks
         * ===============================
         */
        connect: {
            options: {
                hostname: 'localhost'
            },
            dev: {
                options: {
                    keepalive: true,
                    livereload: 35729,
                    port: 8001,
                    base: '<%= paths.src %>',
                    open: '<%= paths.devDomain %><%= paths.email %>'
                }
            },
            dist: {
                options: {
                    port: 8000,
                    keepalive: true,
                    livereload: false,
                    base: '<%= paths.dist %>',
                    open: '<%= paths.distDomain %><%= paths.email %>'
                }
            }
        },

        /**
         * Cleanup Tasks
         * ===============================
         */
        clean: {
            dist: ['<%= paths.dist %>']
        },

        /**
         * Images Optimization Tasks
         * ===============================
         */
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.src %>/<%= paths.images %>',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= paths.dist %>/<%= paths.images %>'
                }]
            }
        },

        /**
         * Copy gif files Tasks
         * ===============================
         */
        copy: {
            gif: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.src %>/<%= paths.images %>',
                    src: ['{,*/}*.gif'],
                    dest: '<%= paths.dist %>/<%= paths.images %>'
                }]
            }
        },

        /**
         * Premailer Parser Tasks
         * ===============================
         */
        premailer: {
            options: {
                baseUrl: '<%= paths.distDomain %>'
            },
            dist: {
                src: '<%= paths.src %>/<%= paths.email %>',
                dest: '<%= paths.dist %>/<%= paths.email %>'
            }
        },

        /**
         * Test Mailer Tasks
         * ===============================
         */
        nodemailer: {
            options: {
                transport: {
                    type: 'SMTP',
                    options: {
                        service: 'Gmail',
                        auth: {
                            user: 'melindaweigert@gmail.com',
                            pass: '7RUff13577'
                        }
                    }
                },
                recipients: [
                    {
                        name: 'Melinda Weigert',
                        email: 'melindaweigert@yahoo.com.au'
                    }
                ]
            },
            dist: {
                src: ['<%= paths.dist %>/<%= paths.email %>']
            }
        }

    });


    grunt.registerTask("preserve-by-custom-tags", "Adds the client css back into the dist.", function() {

        var rx = new RegExp( "<!--GRUNT_COPY_CLIENT_START-->" + "[\\d\\D]*?" + "<!--GRUNT_COPY_CLIENT_END-->", "g" )
            ,snippet = grunt.file.read( "app/generic.html" ).match( rx ).toString()
            ,file = "dist/"+grunt.config.data.paths.email;

        grunt.file.write( file, grunt.file.read( file ).replace(rx, snippet) );
    });


    [
        'grunt-contrib-connect',
        'grunt-contrib-watch',
        'grunt-contrib-compass',
        'grunt-contrib-imagemin',
        'grunt-contrib-copy',
        'grunt-contrib-clean',
        'grunt-premailer',
        'grunt-nodemailer',
    ].forEach(grunt.loadNpmTasks);

    grunt.registerTask('default', 'dev');

    grunt.registerTask('dev', [
        'compass:dev',
        'connect:dev',
        'watch'
    ]);

    grunt.registerTask('dist', [
        'clean',
        'imagemin',
        'copy',
        'compass:dist',
        'premailer:dist',
        'preserve-by-custom-tags', // by Jim Doyle
        'connect:dist'
    ]);

    grunt.registerTask('send', [
        'clean',
        'imagemin',
        'copy',
        'compass:dist',
        'premailer:dist',
        'nodemailer'
    ]);

};
